from collections.abc import MutableMapping
import copy
import plotly.colors
from itertools import accumulate


def plotly_colorscales():
    color_scales = {}
    color_list = plotly.colors.sequential.swatches().to_dict()['data']

    for colorscale_idx in range(len(color_list)):
        name = color_list[colorscale_idx]['y'][0].lower()
        data = color_list[colorscale_idx]['marker']['color']
        color_scales[name] = data
        
    return color_scales


class Log(MutableMapping):
    """
    reference: https://stackoverflow.com/questions/3387691/how-to-perfectly-
    override-a-dict
    Log of prior games, organized by type of agent and encounter count.
    Encounters are logged as (self, opponent) tuples.
        {
            Opposite_Agent_Type: {
                Encounter_1 : [(iter1), (iter2)],
                ...
            }
        }
        
    Additional Notes:
    ----------
    Score value for action pairs are stored within log, since log tracks all encounter info
    There are 2 types of scores:
    Flattening the detailed scores in the right order yields running score
    """
    def __init__(self, **kwargs):
        self._payoff_matrix = kwargs.get('payoff_matrix', None)
        
        self._log = dict()
        self._log_score = dict()
        self._current_score = 0
        self._encounter_order = []
            
        self.current_opp = None
        self.current_encounter = None

    def __getitem__(self, key):
        return self._log[key]
    def __setitem__(self, key, value):
        self._log[key] = value
    def __delitem__(self, key):
        del self._log[key]
    def __iter__(self):
        return iter(self._log)
    def __len__(self):
        return len(self._log)
    def __repr__(self):
        return str(self._log)    
    
    
    def current(self):
        return self.current_opp, self.current_encounter

    def set_current(self, opp_agent_type, current_encounter):
        _check_agent_existence(opp_agent_type)
        _check_encounter_suffix(current_encounter)
        
        self.current_opp = opp_agent_type
        self.current_encounter = current_encounter

        
    @property
    def score(self):
        return self._log_score
    
    @property
    def current_score(self):
        return self._current_score
    
    @property
    def seq_score(self):
        res = []
        try:
            for encounter in self._encounter_order:
                agent_key, encounter_key = encounter.split("-")
                res.extend(
                    self._log_score[agent_key][encounter_key]
                )
            res = list(accumulate(res))
        except: pass
        finally: return res
        
        
    # Utility
    def actions(self, for_self=False, opp_agent_type=None, encounter_integer=None):
        """
        Returns the actions history/encounter array for a given side.
        
        Parameters
        ----------
        for_self: boolean
            Returns a list containing the sequence of moves 
            played either by the opponent or by the agent
        """
        opp_agent_type = self.current_opp if opp_agent_type is None else opp_agent_type
        encounter_integer = self.current_encounter if encounter_integer is None else encounter_integer
        
        _check_agent_existence(opp_agent_type)
        _check_encounter_suffix(encounter_integer)
        
        encounter_key = f'Encounter_{encounter_integer}'
        index = 0 if for_self else 1
        _array = []
        
        try:  # accommodating for log creation order. key error -> empty log
            _array[:] = map(lambda x: x[index], self._log[opp_agent_type][encounter_key])
            # possibly equiv [x[index] for x in self._log[opp_agent_type][encounter_key]]
        except KeyError:
            _array = []
            
        return _array
    
    def to_dict(self):
        as_dict = {agent: encounter for agent, encounter in self.items()}
        as_dict = copy.deepcopy(as_dict)  # Reset pointers
        
        return as_dict
        
    def latest_encounter_with(self, opp_agent_type=None):
        """
        Returns encounter with biggest suffix.
        Mainly used for creating new interactions.
        """
        opp_agent_type = self.current_opp if opp_agent_type is None else opp_agent_type
        
        try:  # If key exists
            encounters_suffix = list(self._log[opp_agent_type].keys())
            encounters_suffix[:] = map(lambda x: int(x.split('_')[-1]), encounters_suffix)
            _max = max(encounters_suffix)
            return int(_max)
        except:
            # print(f"Key '{opp_agent_type}' not found.")
            return 0  # Sets default encounter to 1 when used a arg
    
        
    # Add information
    def register_agent_encounter(self, opp_agent_type=None, encounter_integer=None):
        """
        Helper function to format the log down to encounter level
        """
        opp_agent_type = self.current_opp if opp_agent_type is None else opp_agent_type
        encounter_integer = 1 if encounter_integer is None else encounter_integer
        encounter_key = f'Encounter_{encounter_integer}'
        
        if opp_agent_type not in self._log:
            self._log[opp_agent_type] = {}
            self._log_score[opp_agent_type] = {}
        if encounter_key not in self._log[opp_agent_type]:
            self._log[opp_agent_type][encounter_key] = []
            self._log_score[opp_agent_type][encounter_key] = []
            
            self._encounter_order.append(f"{opp_agent_type}-{encounter_key}")
    
    def update(self, tuple_to_add, opp_agent_type=None, encounter_integer=None):
        """
        Adds to the log, requires that every level of the dict exists.
        Encounter chronology not enforced.
        * Updates `score` as well
        """
        opp_agent_type = self.current_opp if opp_agent_type is None else opp_agent_type
        encounter_integer = self.current_encounter if encounter_integer is None else encounter_integer

        _check_agent_existence(opp_agent_type)
        _check_encounter_suffix(encounter_integer)
        _check_log_record_format(tuple_to_add, encounter_integer)
        encounter_key = f'Encounter_{encounter_integer}'
        
        self.register_agent_encounter(opp_agent_type, encounter_integer)
        tuple_numeric_value = self._payoff_matrix[tuple_to_add[0]][tuple_to_add[1]]
        
        self._log[opp_agent_type][encounter_key].append(tuple_to_add)
        self._log_score[opp_agent_type][encounter_key].append(tuple_numeric_value)
        
        self._current_score += tuple_numeric_value

        

class Agent:
    """
    Abstract class for an entity, behavior/strategy undefined.

    Parameters
    ----------
    start_score : numeric
        Gives the agent an initial score.
        History is tracked using a list.
    log_priors : nested dictionaries
        Log of prior games, organized by type of agent and encounter count.
        Encounters are logged as (self, opponent) tuples.
            {
                Opposite_Agent_Type: {
                    Encounter_1 : [
                        (iter1),
                        (iter2)
                    ],
                }

            }
    """
    
    strategies = set()
    strategies.add('agent')  # Testing purposes
    
    
    
    def __init__(self, **kwargs):
        self.graph_colors = plotly_colorscales()
        self._kwargs = kwargs  # Hack to fix sandbox population construction
        self.score = kwargs.get('score', 0)
        self.log = Log(**kwargs)
        self.private_id = kwargs.get('private_id', None)
        self.extended_id = f'{self.__class__.__name__}_{self.private_id}'
        self.payoff_matrix = kwargs.get('payoff_matrix', None)
        self.colors = self.graph_colors['plotly3']  # default plotly colors
        
#         self.alive = True
    
    def apply_strategy(self):  # temporary name
        raise NotImplementedError
    
    def kill(self):
        self.alive = False
    
def _check_agent_existence(agent_name):
    if agent_name not in Agent.strategies:
        raise ValueError(f"Given agent '{agent_name}' not registered as a valid agent.")


def _check_encounter_suffix(suffix):
    if not isinstance(suffix, int):
        raise TypeError("Encounter suffix is not an integer.")


def _check_log_priors_format(priors):
    """
    Checks the format of priors fed to the Log constructor.
    """
    if not isinstance(priors, (dict, Log)):
        raise TypeError("Expected priors to be a dictionary or a Log object.")
        
    if isinstance(priors, Log):
        priors = priors.to_dict()
        
    for agent in priors:
        if type(agent) is not str:
            raise TypeError(f"Not all agents are of type string: {agent}")
        if type(priors[agent]) is not dict:
            raise TypeError(f"Encounter history for {agent} must be a dictionary.")
        for encounter, history in priors[agent].items():
            if type(encounter) is not str:
                raise TypeError(f"Encounter key not string: {encounter}")
            if not encounter.split('Encounter_')[-1].isnumeric():
                raise ValueError(f"Encounter key suffix not numeric: {encounter}. Expected 'Encounter_#'")
            if type(history) is not list:
                raise TypeError(f"History for {encounter} is not a list.")
            if len(history) == 0:
                raise ValueError(f"Empty encounter list detected for {encounter}")
            for record in history:
                _check_log_record_format(record, encounter)
                
def _check_log_record_format(record, encounter=None):
    """
    Made into separate function for update method
    """
    if encounter is None:
        encounter = 'Error_None'
    else:
        encounter = f'Encounter_{encounter}'  # Encounter is fed in as int.
    if type(record) is not tuple:
        raise TypeError(f"{record} from {encounter} is not a tuple.")
    if len(record) != 2:
        raise ValueError(f"Wrong length found for {record} from {encounter}: {len(record)}.")
    for action in record:
        if type(action) is not str:
            raise TypeError(f"Action {action} from {encounter} is not a string.")
            # Not verifying input quality other than types for flexibility.
            # Validity checker to be implemented later?
