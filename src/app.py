#!/usr/bin/env python
# coding: utf-8

# In[1]:


import derived
from sandbox_ref import Sandbox ##

import pandas as pd
import numpy as np
import json

import plotly.graph_objects as go
import plotly.express as px

from jupyter_dash import JupyterDash
import dash
import dash_table

from dash.dependencies import (
    Input, Output, State,
    MATCH, ALL
)

import dash_core_components as dcc
import dash_html_components as html


agent_types = derived.clsmembers
default_payoff_profiles = {}

visible = {"display": "inline"}
invisible = {"display": "none"}

matrix_warning = ["Notice: The app currently only supports symmetric actions matrices, and input sanitation is not yet implemented.", 
                  html.Br(), "Click to dismiss."]
blurb = dcc.Markdown("""
Working demo with restrictions on graph and agents.  
**Graphs**: 2 simple graphs, TODO: layout + interesting reprs.  
**Matrices**: TODO: defaults missing  
**Agents**: Names are specific to a prisoner's dilemma with action set {C,D},
so while the matrix maker is flexible, agent strategies do not yet match that flexibility.
""")

todo_list = dcc.Markdown("""
    **ONE DAY THERE WILL BE PROPER CSS**  
""")


app = dash.Dash(__name__)

app.title = "Woah"
server = app.server

def StratSyncedDropdown(_id, placeholder):
    @app.callback(
        Output(_id, "options"),
        Input("payoff-profiles", "modified_timestamp"),
        State("payoff-profiles", "data")
    )
    def update_strat_ddowns(_, data):
        if _ is None:
            raise dash.exceptions.PreventUpdate
        options = [{"label": payoff_name, "value": payoff_name}
                for payoff_name in data.keys()]
        return options
    
    return dcc.Dropdown(
        id=_id,
        searchable=False,
        placeholder=placeholder
    )


# In[3]:


def AgentSlider(agent):  # slider only
    return html.Div(
        style={"padding": "20px 10px 0px 4px"},
        children= [
            html.P(
                className="agent-slider", children=[
                    html.Span(f"{agent}: "),
                    html.Span(id={"cluster": "agent-tab", "element": "count", "agent-type": agent}),
                    StratSyncedDropdown(
                        _id={"cluster": "agent-tab", "element": "strat-ddown", "agent-type": agent},
                        placeholder="Select Strategy"
                    
                    )
                ]
            ),
            html.Div(
                style={"margin-left": "6px"}, 
                children=dcc.Slider(
                    id = {"cluster": "agent-tab", "element": "slider", "agent-type":agent},
                    min = 0, max = 50, value = 0,
                    step = 1,
                    marks = {str(i): str(i) for i in [0,10,20,30,40,50]},
                    persistence_type = "memory"
                )
            ),
        ]
    )

def DismissibleButton(msg, _id):
    @app.callback(
        Output(_id, "hidden"),
        Input(_id, "n_clicks")
    )
    def hide_matrix_wrng_button(n_clicks):
        return True if n_clicks > 0 else False

    return html.Button(
        className="spans",
        id=_id,
        children=msg,
        n_clicks=0
    )


# In[4]:


app.layout = html.Div(
    children=[
        html.Div(
            id="hiddenDiv",
            children = [
                html.Div(
                    children=[
                        dcc.Store(id="payoff-profiles", data=default_payoff_profiles),
                        dcc.Store(id="sim-data-seq"),
                        dcc.Store(id="sim-data-det")
                    ]
                ),
            ]
        ),
        html.Div(
#             id="head",
            children=[
                html.H2("Lorem Ipsum"),
                html.P(blurb)
            ]
        ),
        html.Div(
#             id="body",
            className="container",
            children=[
                html.Div(
                    id="optionsPanel",
                    children=[  # OPTIONS WITH TABS
                        dcc.Tabs(
                            id="tabs",
                            children = [
                                dcc.Tab(  # General setup
                                    id="payoffs-tab",
                                    label = "Payoffs",
                                    children = [
                                        html.Section(
                                            children = [
                                                DismissibleButton(
                                                    msg=matrix_warning,
                                                    _id="matrix-restrictions-button"
                                                ),
                                                html.H4("Matrix Configuration"),
                                                html.Div(  # Create new payoffs
                                                    children=[
                                                        html.Div(  # new matrix
                                                            children= [
                                                                html.H5("Create a new payoff matrix"),
                                                                html.Span(  # take name
                                                                    children=[
                                                                        "New payoff name", html.Br(),
                                                                        dcc.Input(
                                                                            id="new-payoff-iname",
                                                                            type="text",
                                                                            placeholder="e.g. Prisoner's dilemma"
                                                                        )
                                                                    ]
                                                                ), html.Br(),
                                                                html.Span(  # take actions
                                                                    children=[
                                                                        "Action set", html.Br(),
                                                                        dcc.Input(
                                                                            id="new-payoff-action-set",
                                                                            type="text",
                                                                            placeholder="comma-separated, e.g. C, D"
                                                                        )
                                                                    ]
                                                                ), html.Br(), # TODO:  allow overwriting
                                                                html.Button(  # submit
                                                                    id={"cluster": "payoff-maker", "element": "button", "has-output": True},
                                                                    className="right",
                                                                    children="Create"
                                                                ),
                                                                dcc.RadioItems(
                                                                    id="overwrite-check",
                                                                    options = [{"label": "overwrite", "value": "T"}]
                                                                )
                                                            ]
                                                        ),
                                                        html.Div(  # view
                                                            className="pad-top",
                                                            children= [
                                                                html.H5("View normal form"),
                                                                html.Span(  # take name
                                                                    children=[
                                                                        StratSyncedDropdown(
                                                                            _id={"cluster": "normal-form", "element":"strat-ddown", "index": 1},
                                                                            placeholder="Payoff as row agent",
#                                                                             options = [{"label": 1, "value": "asd"}]
                                                                        ),
                                                                        StratSyncedDropdown(
                                                                            _id={"cluster": "normal-form", "element":"strat-ddown", "index": 2},
                                                                            placeholder="Payoff as column agent",
#                                                                             options = [{"label": 1, "value": "asd"}]
                                                                        )
                                                                    ] ##
                                                                ),
                                                                html.Button(  # submit
                                                                    id={"cluster":"normal-form", "element": "button", "has-output": True},
                                                                    className="right",
                                                                    children="Display"
                                                                ),
                                                            ]
                                                        ),
                                                    ]
                                                )
                                            ]
                                        )
#                                         https://dash.plotly.com/datatable/editable
                                    ]
                                ),
                                dcc.Tab(  # AGENT CONFIG
                                    id="agent-tab",
                                    label = "Agent Config",
                                    children = [
                                        html.Span(
                                            children= [
                                                dcc.Input(
                                                    id="num-turns",
                                                    className="agent-slider",
                                                    type="number",
                                                    min=0, max=999,
                                                    placeholder="Number of "
                                                ),
                                                html.Span(" turns")
                                            ]
                                        ),
                                        html.Div(
                                            children = [AgentSlider(agent) for agent in agent_types.keys()]
                                        ),

                                        html.Button(
                                            "Start",
                                            id={"cluster":"graphs", "element": "button", "has-output": True},
                                            className="right confirm-btn",
                                            n_clicks=0,
                                            style=dict(display="table-cell")
                                        )
                                    ],
                                ),
                                dcc.Tab(  # EXTRA SETTINGS
                                    label = "Hopes and dreams",
                                    children = [
                                        todo_list
                                    ],
                                )
                            ]
                        )
                    ]
                ),
                html.Div(
                    id="bigData",
                    children=[
                        html.Div(
                            id="graphs", style=invisible,
                            children=[
                                dcc.Loading(
                                    id="load-graph1",
                                    children=[
                                        dcc.Graph(id="graph1"),
                                        dcc.Graph(id="graph2"),
                                    ]
                                )
                            ]
                        ),
                        html.Div(
                            id="payoff-maker", 
                            style=invisible,
                            children=[
                                html.Div(
                                    id="div-table",
                                    children=[
                                        html.Div(id="table-data-cb-errors"),
                                        dash_table.DataTable(
                                            id="payoff-maker-table",
                                            editable=True
                                        )
                                    ]
                                ),
                                html.Div(
                                    id="div-payoff-maker-btn"
                                )
                            ]
                        ),
                        html.Div(
                            id="normal-form", 
                            style=invisible,
                            children=[
                                html.Div(id="normal-form-cb-errors"),
                                dash_table.DataTable(
                                    id="normal-form-table",
                                    editable=False
                                )
                            ]
                        
                        ),
                    ]
                )
            ]
        ),
        html.Div(
            id="footer",
            className="container",
#             children="lorem"
        )
    ]
)


@app.callback(
    Output({"cluster": "agent-tab", "element": "count", "agent-type":MATCH}, "children"),
    Input({"cluster": "agent-tab", "element": "slider", "agent-type":MATCH}, "value"),
)
def update_slider_val(val):
    return val


# In[5]:


# Table data section
@app.callback(
    [
        Output("table-data-cb-errors", "children"),
        Output("table-data-cb-errors", "style"),
        Output("payoff-maker-table", "columns"),
        Output("payoff-maker-table", "data"),
        Output("div-payoff-maker-btn", "children")
    ],
        Input({"cluster": "payoff-maker", "element": "button", "has-output": True}, "n_clicks"),
    [
        State("new-payoff-iname", "value"),              
        State("new-payoff-action-set", "value"),               
        State("payoff-profiles", "data"), # input checking               
        State("overwrite-check", "value"),
    ]
)
def update_table_data(n_clicks, payoff_name, input_actions, payoff_dict, overwrite):
    """
    {O(0), O(1)} : handle errors
    {O(2), O(3)} : Update DataTable
    {O(4)}       : Create confirm button.
    """
    if n_clicks is None:
        raise dash.exceptions.PreventUpdate
    if payoff_name is None:
        raise dash.exceptions.PreventUpdate
    if input_actions is None:
        raise dash.exceptions.PreventUpdate
    
    # Check overwrite
    if payoff_name in payoff_dict.keys() and overwrite != "T":
        return "Error: Payoff already exists. Enable overwrite if you want to use this name.", visible, None, None, []
    
    # Input sanitization
    action_set = [action.strip() for action in input_actions.split(",") if action.strip() != ""]
    if len(set(action_set)) != len(action_set):
        return "Error: Duplicate action found.", visible, None, None, False
    if any("_" in action for action in action_set):
        return "Error: Do not put underscores in action names.", visible, None, None, []
    
    DataTableColumns =(
        [{"name": "index", "id": f"{payoff_name}_index", "type": "text", "editable": False}] + 
        [{"name": action, "id": f"{payoff_name}_{action}", "type": "numeric"} for action in action_set]
    )
    DataTableData = pd.DataFrame(
            0, 
            index=np.repeat(f"{payoff_name}_index", len(action_set)), 
            columns=[f"{payoff_name}_{action}" for action in action_set] ###
        ).to_dict("records")

    # add index id to data dict
    for row, action in zip(DataTableData, action_set):
        row[f"{payoff_name}_index"] = action
    
    ConfirmButton = html.Button(
        id={"payoff-name": payoff_name, "element":"confirm-btn"},
        className="right confirm-btn",
        children="Register"
    )
        
    return None, invisible, DataTableColumns, DataTableData, ConfirmButton

@app.callback(
    Output({"payoff-name": MATCH, "element":"confirm-btn"}, "style"),
    Input({"payoff-name": MATCH, "element":"confirm-btn"}, "n_clicks"),
)
def hide_confirm_btn(n_clicks):
    if n_clicks is None:
        raise dash.exceptions.PreventUpdate
    return {"display": "none"}

@app.callback(
    Output("payoff-profiles", "data"),
    Input({"payoff-name": ALL, "element":"confirm-btn"}, "n_clicks"),
    State("payoff-maker-table", "data"),
    State("payoff-profiles", "data"),
)

def update_payoff_profiles(_, df_data, payoff_dict):
    try:
        if _[0] is None:
            raise dash.exceptions.PreventUpdate
    except IndexError: pass
    if df_data is None:
        raise dash.exceptions.PreventUpdate
        
    # parse table data
    payoff_name = ""
    parsed_data = {}
    for row in df_data:
        temp_row = {}
        idx_key = None
        for old_key in row.keys():
            payoff_name, new_key = old_key.split("_")
            
            if new_key == "index":
                idx_key = old_key
                parsed_data[row[old_key]] = {}
            else:
                temp_row[new_key] = row[old_key]
        parsed_data[row[idx_key]] = temp_row
    
    payoff_dict[payoff_name] = parsed_data
    return payoff_dict


# In[6]:


# View normal form
@app.callback(
    [
        Output("normal-form-cb-errors", "children"),
        Output("normal-form-cb-errors", "style"),
        Output("normal-form-table", "columns"),
        Output("normal-form-table", "data"),
    ],
        Input({"cluster":"normal-form", "element": "button", "has-output": True}, "n_clicks"),
    [
        State("payoff-profiles", "data"),
        State({"cluster": "normal-form", "element":"strat-ddown", "index": ALL}, "value")
    ]
)
def display_payoffs(n_clicks, payoff_dict, _ddowns):
    if n_clicks is None:
        raise dash.exceptions.PreventUpdate
    if payoff_dict is None:
        raise dash.exceptions.PreventUpdate
        
    ddowns = [ddown for ddown in _ddowns if ddown is not None]
    
    if len(ddowns) == 0:
        return "Error: No payoff chosen.", visible, None, None
    elif len(ddowns) == 1:
        payoff_name = ddowns[0]
        pmatrix = payoff_dict[payoff_name]
        cols, data = datatable_outputs([pmatrix])
    elif len(ddowns) == 2:
        payoffs = [payoff_dict[payoff_name] for payoff_name in ddowns]
        if incompatible(payoffs):
            return "Error: Payoff mismatch. Multi-input individual view to be implemented later. Single input works."
        cols, data = datatable_outputs(payoffs)
    else:
        return html.Pre(f"Unexpected error:{_ddowns}"), visible, None, None
    return None, invisible, cols, data

def incompatible(payoffs):
    a, b = payoffs
    a = pd.DataFrame(a)
    b = pd.DataFrame(b)
    
    # exists NaN if incompatible tables
    return (a + b).isnull().values.any()  


def datatable_outputs(payoffs: list, external_id=None):
    def merge_payoffs(payoffs):
        """Assumes compatible"""
        a, b = payoffs
        a = pd.DataFrame(a).applymap(str)
        b = pd.DataFrame(b).applymap(str)

        return (a + ", " + b)
    _id = "index" if external_id is None else external_id
    
    if len(payoffs) == 2: 
        df = merge_payoffs(payoffs)
    else:
        df = pd.DataFrame(payoffs[0]).applymap(str)
        
    cols = ([{"name": "index", "id": _id, "type": "text"}] + 
            [{"name": i, "id": i} for i in df.columns])
    data = df.to_dict("records")
    for row, action in zip(data, df.columns):
        row[f"index"] = action
    
    return cols, data


# In[7]:


# @app.callback(
#     Output("load-graph1", "children"),
# )
# simmy
@app.callback(
    Output("graph1", "figure"),
    Output("graph2", "figure"),
    Input({"cluster":"graphs", "element": "button", "has-output": True}, "n_clicks"),
    [
        State("payoff-profiles", "data"),
        State("num-turns", "value"),
        State({"cluster": "agent-tab", "element": "strat-ddown", "agent-type": ALL}, "value"),
        State({"cluster": "agent-tab", "element": "slider", "agent-type":ALL}, "value")
    ],
    prevent_initial_call=True
)
def compute_sim_data(n_clicks, payoff_dict, turns, *args):
    if n_clicks is None:
        raise dash.exceptions.PreventUpdate
    states = dash.callback_context.states_list
    format_states = {}
    
    for strat_payload in states[-2]:
        agent_type = strat_payload["id"]["agent-type"]
        try:
            strat = strat_payload["value"]
        except:
            # unspecified strat
            continue
        if agent_type not in format_states:            
            format_states[agent_type] = {}
        format_states[agent_type]["strat"] = strat
        
    for count_payload in states[-1]:
        agent_type = count_payload["id"]["agent-type"]
        count = count_payload["value"]
        if count == 0:
            continue
        format_states[agent_type]["count"] = count
    
    sb_input = {}
    for agent_type in format_states:
        payoff_matrix = payoff_dict[format_states[agent_type]["strat"]]
        agent = getattr(derived, agent_type)
        key = agent(
            payoff_matrix=payoff_matrix
        )
        
        sb_input[key] = format_states[agent_type]["count"]
        
    sb = Sandbox(sb_input, upper=True, shuffle=True)
    sb.generate_data(turns)
    return sb.plot.plt_seq_scores_by_instance(), sb.plot.plt_seq_scores_by_type()


# In[8]:


# dispatcher
@app.callback(
    [
        Output("graphs", "style"),
        Output("payoff-maker", "style"),
        Output("normal-form", "style"),
    ],
    Input({"cluster": ALL, "element":"button", "has-output":True}, "n_clicks")
)

def dispatcher(buttons):
    ctx = dash.callback_context
    mask = {
        "graphs"      : [visible, invisible, invisible],
        "payoff-maker": [invisible, visible, invisible],
        "normal-form" : [invisible, invisible, visible]
    }
    
    
    if not ctx.triggered:
        raise dash.exceptions.PreventUpdate
        
    button_id = ctx.triggered[0]["prop_id"].split(".")[0]
    btn_cluster = json.loads(button_id)["cluster"]
    return mask[btn_cluster]


# In[9]:

if __name__ == "__main__":
    app.run_server()


# In[ ]:




