import pandas as pd
import numpy as np
import itertools

# TODO: payoff presets (as cls methods to avoid init?)
class PayoffMaker:
    
    def __init__(self, actions_self, actions_opp=None):
        self.actions_self = actions_self
        self.actions_opp = actions_self if actions_opp is None else actions_opp
    
        self._df = pd.DataFrame(np.nan, index=self.actions_self, columns=self.actions_opp)
        
        self.fill_cells()
        
        self.to_dict()
    
    def fill_cells(self):
        product = []
        product[:] = reversed([i for i in itertools.product(self.actions_self, self.actions_opp)])
        
        while len(product) != 0:
            coord = product.pop()
            self._df.loc[coord] = float(input(f"Desired payoff for action '{coord}': "))
            
        print(f"\n{self._df}\n")
    
    def to_dict(self):
        return self._df.to_dict()
    
    def zero_sum(self):
        return self._df.applymap(lambda x: -x)