import numpy as np
import pandas as pd

import itertools
from tqdm.notebook import tqdm

import plotly.graph_objects as go
from plotly.subplots import make_subplots
from ipywidgets import HBox

from agent import Agent

class SandboxPlotting():
    """
    Composition obj for sandbox, hosts prep figures to avoid
    cluttering Sandbox namespace
    
    https://stackoverflow.com/questions/47797383/plotly-legend-next-to-each-subplot-python
    ^ Depending on the graphs, if legends are not compatible.
    
    """
    # TODO(events) either in here or in Sandbox,
    # only allow plot re-generation if data has changed.
    def __init__(self, *args, **kwargs):
        self._actions = kwargs.get('actions', None)
        self._detailed_scores = kwargs.get('scores', None)
        self._running_scores = kwargs.get('running', None)    
    
    def __call__(self):
        """
        As of yet, only the simple summary implementation exists.
        When sufficient agents are created, will need to make a new subplot accommodating the figs.
        """
        f1 = go.FigureWidget(self.plt_running_scores_by_instance())
        f1.update_layout(
            width = 500,
            legend_orientation = 'h',
            legend_y = 1.1,
            legend_title_text = '',
            legend_bgcolor = 'rgba(0,0,0,0)',
        )
        
        f2 = go.FigureWidget(self.plt_running_scores_by_type())
        f2.update_layout(
            margin = dict(l = 0)
        )
        
        # Removed centered main y-axis title
        for annotation in f2.layout.annotations:
            if annotation['text'] == 'Score':
                annotation['text'] = '' 
        
        return HBox([f1,f2])
        
        
    def update(self, **kwargs):
        self.__init__(**kwargs)
        self._accumulate_detailed_score()
    
    def _set_cm(self, cm):  # Should be set in sandbox constructor
        self._color_map = cm
    
    def _set_types(self, types):
        self._agent_types = types

        
    # Plot functions
    def plt_running_scores_by_instance(self):
        """
        Plots the running score of each agent over time (each step is an interaction)
        Agents are grouped by type. 
        """
        fig = go.Figure()
        
        for agent_type in self._running_scores:
            first_agent_num = True
            for agent_num in range(len(self._running_scores[agent_type])):
                show_legend_bool = True if first_agent_num else False
                first_agent_num = False  # Toggle off after first assignment

                fig.add_scatter(
                    y = self._running_scores[agent_type][agent_num], 
                    text = f'{agent_type} {agent_num}',
                    marker_color = np.random.choice(self._color_map[agent_type]),
                    
                    # Avoid duplicate handle/labels
                    name = agent_type * show_legend_bool,  
                    legendgroup = agent_type,
                    showlegend = show_legend_bool
                )
        fig.update_layout(
            title = {
                "text": "Score Over Time by Agent Instance",
                "x": 0.5,
            },
            xaxis_title = "Time (# of Interactions)",
            yaxis_title = "Score",
            legend_title = "Agent Type",
        )
        
        return fig
    
    
    def plt_running_scores_by_type(self, verbose_text=False, **kwds):
        """
        Makes a subplot for each agent type given, plot running score of each
        instance provided it is not identical to another one.
        remove duplicates -> reduce graph load on deterministic encounters
        
        Temporary (?) arg: verbose_text only affects add_scatter name component
        """
        deduped_history = self._detailed_running_scores
        subplot_quadrant, m_row, m_col = self._make_subplot_coordinates_with_rowcol(self._agent_types)
        fig = make_subplots(
            m_row, m_col,
            x_title = "Time (# of Interactions)",
            y_title = "Score",
            shared_xaxes = True, shared_yaxes = True,
            subplot_titles = tuple(subplot_quadrant.keys()),
            **kwds
        )

        
        # First item gets group and legend settings
        already_a_legendgroup = set()
        for agent_type in deduped_history.keys():
            for opp_agent in deduped_history[agent_type].index.get_level_values(0):
                if opp_agent in already_a_legendgroup:
                    show_legend_bool = False
                else:
                    show_legend_bool = True
                    already_a_legendgroup.add(opp_agent)

                rows = deduped_history[agent_type][opp_agent]

                rows.map(lambda x:
                fig.add_scatter(
                    y = x,
                    text = f"{agent_type * verbose_text} {'vs' * verbose_text} {opp_agent}",
                    marker_color = np.random.choice(self._color_map[opp_agent]),
                    name = opp_agent,
                    legendgroup = opp_agent,
                    showlegend = show_legend_bool,
                    row = subplot_quadrant[agent_type][0], col = subplot_quadrant[agent_type][1],
                    **kwds
                ))
        fig.update_layout(
            title = {
                "text": "Score Over Time by Agent Type",
                "x": 0.5,
            },
            legend_title = "Agent Type",
        )
        
        # Give each subplot ticks
        for xaxis_attr in fig.layout:
            if xaxis_attr.startswith('xaxis'):
                fig.layout.update({f'{xaxis_attr}.showticklabels' : True})
        
        
        return fig

    # Helpers
    def _accumulate_detailed_score(self):
        """
        Helper functions for plotting agent vs agent summaries.
        Removes duplicate histories
        """
        def try_accumulate(li):
            """
            Handles error from None cells caused by array size mismatch in construction
            """
            try:
                return list(itertools.accumulate(li))
            except: 
                return []
            
        frame_from_dict = pd.DataFrame.from_dict(self._detailed_scores, orient='index')
        expanded_dfs = []
        
        for col in frame_from_dict.columns:
            expanded_dfs.append(
                pd.DataFrame(frame_from_dict[col].to_list(), index=frame_from_dict.index)
            )
            
        df = pd.concat(expanded_dfs, axis=1, keys = frame_from_dict.columns)
        df = df.applymap(try_accumulate)
        
        history_by_agent = {}
        for agent_type in df.index:
            history_by_agent[agent_type] = df.loc[agent_type].copy()  # Breaking up each row

        # Managing duplicates
        for agent_type in history_by_agent:
            temp_df = history_by_agent[agent_type].copy()
            temp_df = temp_df.map(tuple)
            temp_df = temp_df.rename('Actions').to_frame().reset_index()
            # Columns are now level_0, level_1, Actions
            temp_df.drop_duplicates(subset=['level_0', 'Actions'], inplace=True)
            temp_df = temp_df[temp_df.Actions != ()]  # Remove ()

            # Formatting back to series
            temp_df.set_index(['level_0', 'level_1'], inplace=True)
            temp_df.index.names = ['', '']
            temp_df.rename({'Actions': ''}, axis=1, inplace=True)

            history_by_agent[agent_type] = temp_df.stack()

        self._detailed_running_scores = history_by_agent
    
    def _make_subplot_coordinates_with_rowcol(self, agents, **kwds):
        """
        Given a list, reshape into a matrix where each item from the list
        maps to the coordinate.
        Absence of args -> closest to square
        """
        if isinstance(agents, (list, tuple)):
            agents = np.unique(np.array(list(agents)))  # Remove dupes + keep order
        elif isinstance(agents, set):
            agents = np.array(list(agents))
        elif isinstance(agents, (np.ndarray)):
            pass
        else:
            raise TypeError

        def matrix_of_agents(agents, **kwds):
            if agents.size == 4:
                max_cols = kwds.get('max_cols', 2)  # 2x2
            else:
                max_cols = kwds.get('max_cols', 3)


            rows = np.ceil(agents.size/max_cols).astype(int)

            mat = np.zeros(rows * max_cols, object)
            mat[:agents.size] = agents

            return mat.reshape(rows,max_cols), int(rows), int(max_cols) #

        def possible_coords(matrix):
            x, y  = np.indices(matrix.shape)
            idx = np.stack([x.ravel(), y.ravel()], axis=1)

            return idx

        sub_coords = {}
        moa, max_row, max_col = matrix_of_agents(agents, **kwds)
        coords = possible_coords(moa)

        for coord in coords:
            if moa[tuple(coord)] in agents.tolist():  # Ignores 0 by matching
                sub_coords[moa[tuple(coord)]] = tuple(i+1 for i in coord)  # subplot indexing starts at 1

        return sub_coords, max_row, max_col

    
    
class Sandbox:
    """
    Object in which simulations are run.

    Parameters
    ----------
    the_snap : dictionary
        Dictionary that defines the (initial) entity set.
        The key must be an instance of the desired agent, and the value is the count.
        Does not accept the same class multiple times with different parameters.
        {
            Cooperative() : 5
        }
    """
    def __init__(self, the_snap, **kwargs):
        _check_sandbox_population_format(the_snap)
        self._agent_types = set()
        self.population = the_snap
        self.plot = SandboxPlotting()
        self.agg_actions = {}
        self.agg_scores = {}
        self.agg_running = {}  # running scores
        
        self._set_matrix_default()
        self._set_interaction_array(**kwargs)
        
        self.color_map = the_snap
        self.plot._set_cm(self.color_map)
        self.plot._set_types(self._agent_types)

    @property
    def population(self):
        return self._population

    @population.setter
    def population(self, the_snap):
        self._population = []
        for agent in the_snap:
            for enum, instance in enumerate(range(the_snap[agent])):
                self._population.append(
                    agent.__class__(
                        **agent._kwargs
                    )
                )
            
            #add to set
            self._agent_types.add(agent.__class__.__name__)
                
                
    @property
    def color_map(self):
        return self._color_map
    
    @color_map.setter
    def color_map(self, the_snap):
        self._color_map = {}
        for agent in the_snap:
            key = agent.__class__.__name__
            value = agent.colors
            self._color_map[key] = value
        
        
    @property
    def matrix(self):
        """
        This property returns the index representation of the interaction matrix,
        Runs the numpy meshgrid function with a default.
        """
        return self._matrix

    def _set_matrix_default(self):
        self._matrix = np.array(np.meshgrid(self._population, self._population, indexing='ij'))

    def override_matrix_custom(self, *xi):
        if len(xi) == 1:
            self._matrix = np.array(np.meshgrid(xi, xi))
        else:
            self._matrix = np.array(np.meshgrid(*xi))

    @property
    def interaction_array(self):
        """
        Returns the array of interactions as described by the interaction matrix
        The contents of this function are described by
        {self.matrix[:,i,j]: for all i,j} where self.matrix is an ixj matrix

        Refers to self.remove_diagonal.
        """
        return self._interaction_array

    def _set_interaction_array(self, **kwargs):
        """
        Given a default matrix, not running either upper or lower
        will make every agent interact twice.
        """
        upper = kwargs.get('upper', False)
        triu_k_offset = kwargs.get('triu_k', 1)
        lower = kwargs.get('lower', False)
        tril_k_offset = kwargs.get('tril_k', -1)
        remove_diagonal = kwargs.get('remove_diagonal', False)
        shuffle = kwargs.get('shuffle', False)

        if sum(bool(kwarg) for kwarg in (upper, lower, remove_diagonal)) > 1:
            raise ValueError("Too many matrix flags enabled.")

        matrix_copy = self.matrix.copy()
        i, j = matrix_copy.shape[1:3]
        mask = np.ones((i, j)).astype(bool)

        if remove_diagonal:
            np.fill_diagonal(mask, False)

        if upper:
            mask = np.triu(mask, triu_k_offset).astype(bool)

        if lower:
            mask = np.tril(mask, tril_k_offset).astype(bool)

        masked_matrix = matrix_copy[:, mask]
        self._interaction_array = np.stack(masked_matrix, axis=1)
        
        if shuffle:
            np.random.shuffle(self._interaction_array)


    def generate_data(self, turns, **kwargs):
        """
        Requires that the update matrix is not empty.
        This function runs the simulation.

        Parameters
        ----------
        turns : integer
            How many times each pair of agents are to interact
        debug : boolean
            Prints agent info
        """
        debug = kwargs.get('debug', False)
        
        if type(turns) is not int:
            raise TypeError("Number of turns is not an integer")
            
        pairs = len(self._interaction_array)
        pbar = tqdm(range(pairs))
        
        for pair in pbar:
            pbar.set_description("Game Progress ")
            
            A = self._interaction_array[pair,0]
            B = self._interaction_array[pair,1]
            
            # Set instance defaults
            A.log.set_current(
                B.__class__.__name__,
                A.log.latest_encounter_with(B.__class__.__name__) + 1
            )
            B.log.set_current(
                A.__class__.__name__,
                B.log.latest_encounter_with(A.__class__.__name__) + 1
            )
            
            if debug:
                print(f"\nMatch {pair}")
                print(f"(A)  Type: {A.__class__.__name__}, id: {A.private_id}, latest: {A.log.latest_encounter_with(B.__class__.__name__)}")
                print(f"     Current: {A.log.current()}")
                print(f"(B)  Type: {B.__class__.__name__}, id: {B.private_id}, latest: {B.log.latest_encounter_with(A.__class__.__name__)}")
                print(f"     Current: {B.log.current()}")
                print()
            
            for turn in range(turns):
                # Game starts
                A_out = A.apply_strategy()
                B_out = B.apply_strategy()
                
                A_save = (A_out, B_out)
                B_save = tuple(i for i in reversed(A_save))  # For 2x2 only
                
                A.log.update(A_save)
                B.log.update(B_save)
                
        self.aggregate_data()
        

    def aggregate_data(self):
        """
        Creates "master" dictionaries for:
            (detailed) actions
            (detailed) scores
            running scores
        """
        for agent in self.population:
            if agent.__class__.__name__ not in self.agg_actions:  # Register primary agent type
                self.agg_actions[agent.__class__.__name__] = {}
                self.agg_scores[agent.__class__.__name__] = {}
    
            for opp_agent in agent.log:
                if opp_agent not in self.agg_actions[agent.__class__.__name__]:  # Register opp agent type given primary
                    self.agg_actions[agent.__class__.__name__][opp_agent] = []
                if opp_agent not in self.agg_scores[agent.__class__.__name__]:  # Register opp agent type given primary
                    self.agg_scores[agent.__class__.__name__][opp_agent] = []
                    
                for encounter in agent.log[opp_agent]:
                    self.agg_actions[agent.__class__.__name__][opp_agent].append(agent.log[opp_agent][encounter])
                    self.agg_scores[agent.__class__.__name__][opp_agent].append(agent.log.score[opp_agent][encounter])
            
            # Separator
            if agent.__class__.__name__ not in self.agg_running:
                self.agg_running[agent.__class__.__name__] = []
            self.agg_running[agent.__class__.__name__].append(
                list(
                    itertools.accumulate(agent.log._running_score)
                )
            )
        
        
        # SandboxPlotting related
        self.plot.update(
            actions = self.agg_actions,
            scores = self.agg_scores,
            running = self.agg_running
        )
            
            
def _check_sandbox_population_format(the_snap):
    if type(the_snap) is not dict:
        raise TypeError("Snap input must be a dictionary.")
    if len(the_snap) == 0:
        raise ValueError("Cannot snap nothing into existence.")

    unique_type = set()
    for agent in the_snap:
        if agent.__class__.__name__ not in Agent.strategies:  # Check is valid strategy
            raise ValueError(f"Agent type {agent}' is not a valid instance.")
        if agent not in unique_type:  # unique strategy for constructor
            unique_type.add(agent)
        else:
            raise ValueError(f"Agent '{agent}' already exists.")
        if type(the_snap[agent]) is not int:
            raise TypeError(f"Number of {agent} needs to be an integer.")
