from agent_ref import Agent
import sys, inspect


class Cooperative(Agent):

    def __init__(self, **kwargs):
        # Add strategy to known list
        super().strategies.add(self.__class__.__name__)
        super().__init__(**kwargs)
        
        color_scheme = kwargs.get('color_scheme', 'burg')
        self.colors = self.graph_colors[color_scheme][2:]
        
    def apply_strategy(self):
        """
        Returns cooperate regardless of input; args and kwargs to not throw error
        """
        return 'C'
        
        
class Cheater(Agent):
    def __init__(self, **kwargs):
        # Add strategy to known list
        super().strategies.add(self.__class__.__name__)
        super().__init__(**kwargs)
        
        color_scheme = kwargs.get('color_scheme', 'algae')
        self.colors = self.graph_colors[color_scheme][5:]
        
        
        
    def apply_strategy(self):
        """
        Returns deviate regardless of input
        """
        return 'D'
    
    
class Copycat(Agent):  # Tit for tat
    def __init__(self, **kwargs):
        super().strategies.add(self.__class__.__name__)
        super().__init__(**kwargs)
    
        color_scheme = kwargs.get('color_scheme', 'teal')
        self.colors = self.graph_colors[color_scheme][3:]
    
    def apply_strategy(self):
        actions = self.log.actions()
        if actions == []:
            return 'C'
        else:
            return actions[-1]
        
        
class Grim(Agent):  # Grim trigger
    def __init__(self, **kwargs):
        super().strategies.add(self.__class__.__name__)
        super().__init__(**kwargs)
        
        color_scheme = kwargs.get('color_scheme', 'magenta')
        self.colors = self.graph_colors[color_scheme][2:]
        
    def apply_strategy(self):
        if all(action == 'C' for action in self.log.actions()):
            return 'C'
        else:
            return 'D'
        
# Leave as last line
clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
clsmembers = sorted([x for x in clsmembers if x[0] != "Agent"])
clsmembers = {x[0]: x[1] for x in clsmembers}
