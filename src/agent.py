from collections.abc import MutableMapping
import copy
import plotly.colors


def plotly_colorscales():
    color_scales = {}
    color_list = plotly.colors.sequential.swatches().to_dict()['data']

    for colorscale_idx in range(len(color_list)):
        name = color_list[colorscale_idx]['y'][0].lower()
        data = color_list[colorscale_idx]['marker']['color']
        color_scales[name] = data
        
    return color_scales
    


class Agent:
    """
    Abstract class for an entity, behavior/strategy undefined.

    Parameters
    ----------
    start_score : numeric
        Gives the agent an initial score.
        History is tracked using a list.
    log_priors : nested dictionaries
        Log of prior games, organized by type of agent and encounter count.
        Encounters are logged as (self, opponent) tuples.
            {
                Opposite_Agent_Type: {
                    Encounter_1 : [
                        (iter1),
                        (iter2)
                    ],
                }

            }
    """
    
    strategies = set()
    strategies.add('agent')  # Testing purposes
    
    
    
    def __init__(self, **kwargs):
        self.graph_colors = plotly_colorscales()
        self._kwargs = kwargs  # Hack to fix sandbox population construction
        self.score = kwargs.get('score', 0)
        self.log = Log(**kwargs)
        self.private_id = kwargs.get('private_id', None)
        self.extended_id = f'{self.__class__.__name__}_{self.private_id}'
        self.payoff_matrix = kwargs.get('payoff_matrix', None)
        self.colors = self.graph_colors['plotly3']  # default plotly colors
        
        self.alive = True
    
    def apply_strategy(self):  # temporary name
        raise NotImplementedError
    
    def kill(self):
        self.alive = False
    
def _check_agent_existence(agent_name):
    if agent_name not in Agent.strategies:
        raise ValueError(f"Given agent '{agent_name}' not registered as a valid agent.")


def _check_encounter_suffix(suffix):
    if not isinstance(suffix, int):
        raise TypeError("Encounter suffix is not an integer.")


def _check_log_priors_format(priors):
    """
    Checks the format of priors fed to the Log constructor.
    """
    if not isinstance(priors, (dict, Log)):
        raise TypeError("Expected priors to be a dictionary or a Log object.")
        
    if isinstance(priors, Log):
        priors = priors.to_dict()
        
    for agent in priors:
        if type(agent) is not str:
            raise TypeError(f"Not all agents are of type string: {agent}")
        if type(priors[agent]) is not dict:
            raise TypeError(f"Encounter history for {agent} must be a dictionary.")
        for encounter, history in priors[agent].items():
            if type(encounter) is not str:
                raise TypeError(f"Encounter key not string: {encounter}")
            if not encounter.split('Encounter_')[-1].isnumeric():
                raise ValueError(f"Encounter key suffix not numeric: {encounter}. Expected 'Encounter_#'")
            if type(history) is not list:
                raise TypeError(f"History for {encounter} is not a list.")
            if len(history) == 0:
                raise ValueError(f"Empty encounter list detected for {encounter}")
            for record in history:
                _check_log_record_format(record, encounter)
                
def _check_log_record_format(record, encounter=None):
    """
    Made into separate function for update method
    """
    if encounter is None:
        encounter = 'Error_None'
    else:
        encounter = f'Encounter_{encounter}'  # Encounter is fed in as int.
    if type(record) is not tuple:
        raise TypeError(f"{record} from {encounter} is not a tuple.")
    if len(record) != 2:
        raise ValueError(f"Wrong length found for {record} from {encounter}: {len(record)}.")
    for action in record:
        if type(action) is not str:
            raise TypeError(f"Action {action} from {encounter} is not a string.")
            # Not verifying input quality other than types for flexibility.
            # Validity checker to be implemented later?
